import { createSlice } from '@reduxjs/toolkit';

export const fakeSlice = createSlice({
    name: 'fake',
    initialState: {
        data: JSON.parse(localStorage.getItem('data') || '{}'),
    },
    reducers: {
        setData: (state, action) => {
            state.data = action.payload;
            localStorage.setItem('data', JSON.stringify(action.payload));
        },
        clearData: (state) => {
            state.data = null;
            localStorage.removeItem('data');
        }
    }
});

export const { setData, clearData } = fakeSlice.actions

export default fakeSlice.reducer