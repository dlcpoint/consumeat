import { configureStore } from '@reduxjs/toolkit';
import fakeSlice from '../slices/fakeSlice';

export default configureStore({
    reducer: {
        fake: fakeSlice,
    },
})