import axios from 'axios';

const API_BASE_URL = 'http://192.168.174.127:3001';

const ConsumEatService = {

    /**
     * Get the product of the given id
     * @param {*} id 
     * @returns 
     */
    async getProduct(id) {
        const response = await axios.get(`${API_BASE_URL}/product/${id}`);

        return response.data;
    },
    
    /**
     * Get all products in the receipt of the given id
     * @param {*} id 
     * @returns 
     */
    async getProductsFromReceipt(id) {
        const response = await axios.get(`${API_BASE_URL}/receipt/${id}`);

        return response.data;
    },

    /**
     * Create a new product
     * @param {*} name Name of the product
     * @param {*} isDLC Does the product has a DLC or a DDM
     * @param {*} date DLC/DDM date
     * @returns 
     */
    async createProduct(name, isDLC, date) {
        const data = { name, isDLC, date };

        const response = await axios.post(`${API_BASE_URL}/product`, data);

        return response.data;
    },
};

export default ConsumEatService;