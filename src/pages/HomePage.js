import { Container } from 'react-bootstrap';

export default function HomePage() {
    const containerStyle = {
        width: '50%',
        height: '50%',
      };

    return (
        <Container className="home-page">
            <Container className="d-flex align-items-center justify-content-center" style={containerStyle}>
                <img src="./consumeat.svg" alt="Image SVG" style={{transform: 'scale(0.5)'}}/>
            </Container>
        </Container>
    );
}
