import { useEffect, useState } from "react";
import { Card, Col, Container, Row } from "react-bootstrap";
import ConsumEatService from "../services/ConsumEatService";
import { useNavigate } from "react-router-dom";


export default function Product() {
    const [product, setProduct] = useState("");
    const [id, setId] = useState();
    const navigate = useNavigate();

    useEffect(() => {
        const urlParams = new URLSearchParams(window.location.search);
        setId(urlParams.get('id'));
        (async () => {
            ConsumEatService.getProduct(urlParams.get('id'))
                .then((product) => setProduct(product))
                .catch((error) => {
                    navigate("/404");
                });
        })();
    }, []);

    return (
        <Container>
            <Row className="justify-content-center">
                <Col xs={11} lg={8}>
                    <Card className="p-4">
                        <Card.Title className="mb-4">Article scanné n°{id} :</Card.Title>
                        <table className="table table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">Nom</th>
                                    <th scope="col">Type</th>
                                    <th scope="col">Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{product.name}</td>
                                    <td>{product.isDLC ? "DLC" : "DDM"}</td>
                                    <td>{product.date}</td>
                                </tr>
                            </tbody>
                        </table>

                        <Container style={{ fontSize: "12px" }}>
                            <p className="mb-5">
                                <span>Plus d'informations : </span>
                                <a href="https://www.service-public.fr/particuliers/vosdroits/F10990" target="_blank">
                                    https://www.service-public.fr/particuliers/vosdroits/F10990
                                </a>
                            </p>
                            <p>* DLC : Date Limite de Consommation</p>
                            <p>* DDM : Date de Durabilité Minimale</p>
                        </Container>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}