import { Container } from "react-bootstrap";

export default function NoPage() {

    return (
        <Container className="no-page">
            <p>404</p>
        </Container>
    );
}
