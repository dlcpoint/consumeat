import { useEffect, useState } from "react";
import { Button, Card, Col, Container, Row } from "react-bootstrap";
import ConsumEatService from "../services/ConsumEatService";
import { Link, useNavigate } from "react-router-dom";
import { BsArrowUp } from "react-icons/bs";
import { formatDistance, formatDistanceToNowStrict, formatRelative } from "date-fns";
import { fr } from 'date-fns/locale';


export default function Receipt() {
    const [productList, setProductList] = useState([]);
    const [id, setId] = useState();
    const navigate = useNavigate();

    useEffect(() => {
        const urlParams = new URLSearchParams(window.location.search);
        setId(urlParams.get('id'));
        (async () => {
            ConsumEatService.getProductsFromReceipt(urlParams.get('id'))
                .then((productList) => {
                    productList.sort((a, b) => {
                        var dateA = new Date(a.date);
                        var dateB = new Date(b.date);
                        return dateA - dateB;
                    });
                    setProductList(productList);    // Sort and set new product list
                })
                .catch((error) => {
                    navigate("/404");
                });
        })();
    }, []);

    const getColorForDate = (date, isDLC) => {
        const now = new Date();
        const productDate = new Date(date);
        const diffTime = productDate.getTime() - now.getTime();
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

        if (!isDLC) {   // Les DDM de périment pas
            return { backgroundColor: "#D7FFC4" };
        }

        if (diffDays < 0) { // Passé
            return { backgroundColor: "#FF5733" };
        } else if (diffDays === 0) { // Aujourd'hui
            return { backgroundColor: "#FFBD33" };
        } else if (diffDays === 1) { // Demain
            return { backgroundColor: "#DBFF33" };
        } else { // Plus tard
            return { backgroundColor: "#75FF33" };
        }
    }

    const getFormattedDate = (date) => {
        const productDate = new Date(date);
        return formatDistanceToNowStrict(productDate, { addSuffix: true, unit: 'day', locale: fr })
    }

    return (
        <Container>
            <Row className="justify-content-center">
                <Col xs={11} lg={8}>
                    <Card className="p-4">
                        <Card.Title className="mb-4">Facture n°{id} :</Card.Title>
                        <table className="table table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Nom</th>
                                    <th scope="col">Type</th>
                                    <th scope="col" className="d-inline-flex align-items-center">
                                        <span>Date</span>
                                        <BsArrowUp style={{ marginLeft: '5px' }} />
                                    </th>
                                    <th scope="col">Détail</th>
                                </tr>
                            </thead>
                            <tbody>
                                {productList.map((product, index) => (
                                    <tr key={index} style={getColorForDate(product.date, product.isDLC)}>
                                        <td>{index}</td>
                                        <td>{product.name}</td>
                                        <td>{product.isDLC ? "DLC" : "DDM"}</td>
                                        <td style={(product.isDLC) ? { fontWeight: "bold" } : {}}>
                                            {product.date}
                                        </td>
                                        <td style={(product.isDLC) ? { fontWeight: "bold" } : {}}>
                                            {getFormattedDate(product.date)}
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>

                        <Row className="justify-content-center">
                            <Col xs={8}>
                                <Container style={{ fontSize: "12px" }}>
                                    <p className="mb-5">
                                        <span>Plus d'informations : </span>
                                        <a href="https://www.service-public.fr/particuliers/vosdroits/F10990" target="_blank">
                                            https://www.service-public.fr/particuliers/vosdroits/F10990
                                        </a>
                                    </p>
                                    <p>* DLC : Date Limite de Consommation</p>
                                    <p>* DDM : Date de Durabilité Minimale</p>
                                </Container>
                            </Col>
                            <Col xs={4}>
                                <Button variant="success">Ajouter à l'agenda</Button>
                            </Col>
                        </Row>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}