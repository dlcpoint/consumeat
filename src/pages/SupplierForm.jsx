import qrious from "qrious";
import { useState } from "react";
import { Button, ButtonGroup, Card, Col, Container, FloatingLabel, Form, Row } from "react-bootstrap";
import ConsumEatService from "../services/ConsumEatService";
import { Link } from "react-router-dom";
import { AiFillPrinter } from 'react-icons/ai';
import { FaFileDownload } from 'react-icons/fa';

export default function SupplierForm() {
    const [productName, setProductName] = useState("");
    const [isDLC, setIsDLC] = useState(true);
    const [productUrl, setProductUrl] = useState();

    let today = new Date();
    let fiveDaysLater = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 6);
    const [date, setDate] = useState(fiveDaysLater.toISOString().slice(0, 10));


    const createProduct = (e) => {
        (async () => {
            ConsumEatService.createProduct(productName, isDLC, date)
                .then((product) => {
                    const qrCode = document.getElementById('qrcode');
                    const url = `http://192.168.174.127:3000/product?id=${product.id}`;
                    new qrious({
                        element: qrCode,
                        value: url,
                        size: 200
                    });
                    setProductUrl(url);
                })
                .catch((error) => {
                    console.log("err", error);
                });
        })();
        e.preventDefault();
    }

    const downloadQrCode = () => {
        const canvas = document.getElementById('qrcode');
        const url = canvas.toDataURL("image/png");
        const link = document.createElement("a");
        link.download = productName + ".png";
        link.href = url;
        link.click();
    }

    const printQrCode = () => {
        const canvas = document.getElementById("qrcode");
        const dataImage = canvas.toDataURL("image/png");
        const fen = window.open();
        fen.document.open();
        fen.document.write("<img src='" + dataImage + "'>");
        fen.document.close();
        fen.onload = setTimeout(function () {
            fen.print();
            fen.close();
        }, 100);
    }

    return (
        <Container>
            <Row className="justify-content-center">
                <Col xs={11} lg={6}>
                    <Card className="p-4">
                        <Card.Title className="mb-4">Saisissez les informations du produit :</Card.Title>
                        <Form onSubmit={createProduct}>
                            <FloatingLabel controlId="floatingInput" label="Product name" className="mb-4">
                                <Form.Control type="text" placeholder="Product name" value={productName} onChange={(e) => setProductName(e.target.value)} required />
                            </FloatingLabel>

                            <Row className="justify-content-center mb-4">
                                <Col xs={4}>
                                    <FloatingLabel label="Type">
                                        <Form.Select value={isDLC} onChange={(e) => setIsDLC(JSON.parse(e.target.value))} required>
                                            <option value={true}>DLC</option>
                                            <option value={false}>DDM</option>
                                        </Form.Select>
                                    </FloatingLabel>
                                </Col>

                                <Col xs={8}>
                                    <FloatingLabel label="Date" value={date} onChange={(e) => setDate(e.target.value)}>
                                        <Form.Control type="date" value={date} onChange={(e) => setDate(e.target.value)} required />
                                    </FloatingLabel>
                                </Col>
                            </Row>

                            <Row className="justify-content-center">
                                <Col xs={12} className="text-center">
                                    <Button variant="secondary" type="submit" className="px-5 py-2">
                                        Ajouter le produit
                                    </Button>
                                </Col>

                                {productUrl !== undefined &&
                                    <hr className="my-5" />
                                }

                                <Col xs={12} className="text-center mb-3">
                                    <canvas id="qrcode" style={{ width: "200px" }}></canvas>
                                </Col>

                                <Col xs={12} className="text-center mb-3">
                                    {productUrl !== undefined &&
                                        <ButtonGroup className="button-group-with-space">
                                            <Button onClick={downloadQrCode} variant="success">
                                                <FaFileDownload size={20} />
                                            </Button>

                                            <Button onClick={printQrCode} variant="secondary">
                                                <AiFillPrinter size={20} />
                                            </Button>
                                        </ButtonGroup>
                                    }
                                </Col>

                                <Col xs={12} className="text-center mb-3">
                                    {productUrl !== undefined &&
                                        <Link to={productUrl} target="_blank">{productUrl}</Link>
                                    }
                                </Col>
                            </Row>

                        </Form>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}