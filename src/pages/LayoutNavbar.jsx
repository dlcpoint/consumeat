import { Container, Nav, Navbar } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

export default function LayoutNavbar() {
    const navigate = useNavigate();

    function handleTo(e) {
        switch (e.target.id) {
            case 'home':
                navigate('/');
                break;
            case 'form':
                navigate('/form');
                break;
        }
    }

    return (
        <>
            <Navbar variant="dark" style={{background: 'linear-gradient(to right, #4a8d31, #93bf1e)'}}>
                <Container>
                    <Navbar.Brand href="/">ConsumEat</Navbar.Brand>
                    <Nav className="me-auto">
                        <Nav.Link href="/">Accueil</Nav.Link>   
                        <Nav.Link href="/form">Formulaire</Nav.Link>
                    </Nav>
                </Container>
            </Navbar>
        </>
    );
}