// import LayoutFooter from "../components/LayoutFooter";
// import LayoutNavbar from "../components/LayoutNavbar";

import LayoutNavbar from "./LayoutNavbar";

export default function Layout({ children }) {

    return (
        <div className="layout">
            {/* <LayoutNavbar /> */}
            <LayoutNavbar />

            <div style={{ minHeight: '100vh', backgroundColor:"whitesmoke" }} className="py-3">
                {children}
            </div>

            {/* <LayoutFooter /> */}
            {/* <p style={{ fontSize: '10px' }}>Créer un composant JSX pour le footer</p> */}
        </div>
    );
}
