import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Layout from './pages/Layout';
import HomePage from './pages/HomePage';
import NoPage from './pages/NoPage';
import './styles/index.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import './i18n';
import { Provider } from 'react-redux';
import store from './providers/store';
import SupplierForm from './pages/SupplierForm';
import Product from './pages/Product';
import Receipt from './pages/Receipt';

export default function App() {
  return (
    <BrowserRouter>
      <Layout>
        <Routes>
          <Route exact path='/' element={<HomePage />} />
          <Route exact path="*" element={<NoPage />} />
          <Route exact path="/404" element={<NoPage />} />
          <Route exact path="/form" element={<SupplierForm />} />
          <Route exact path="/product" element={<Product />} />
          <Route exact path="/receipt" element={<Receipt />} />
        </Routes>
      </Layout>
    </BrowserRouter>
  );
}

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Provider store={store}>
    <App />
  </Provider>
);